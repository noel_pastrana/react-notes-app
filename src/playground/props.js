class IndecisionApp extends React.Component {
    render() {
        const title = "Indecision";
        const subtitle = 'Put your life in the hands of a computer';
        const options = ['thing one', 'thing two', 'thing four'];

        return (
            <div>
                <Header title={title} subtitle={subtitle}/>
                <Action/>
                <Options options={options}/>
                <AddOption/>
            </div>
        );
    }
}

class Header extends React.Component {
    render() {
        return (
            <div>
                <h1>{this.props.title}</h1>
                <h2>{this.props.subtitle}</h2>
            </div>
        )
    }
}


class Action extends React.Component {
    handlePick() {
        alert('hadnlePick');
    }
    render() {
        return (
            <div>
                <button onClick={this.handlePick} className="btn btn-primary">What should I do?</button>
            </div>
        );
    }
}

class Options extends React.Component {

    constructor(props) {
        super(props);
        // this.handleRemoveAll = this.handleRemoveAll.bind(this);
        this.props.options;
        this.handleRemoveAll();
    }

    handleRemoveAll() {
        // alert(this.props.options);
        console.log('this.props.options');
        console.log(this.props.options);

    }
    
    render() {
        return (
            <div>
                <button onClick={this.handleRemoveAll.bind(this)} className="btn btn-danger">Remove All(Binded)</button>
                <button onClick={this.handleRemoveAll} className="btn btn-danger">Remove All(Unbound)</button>

                {
                    this.props.options.map((option) => <p key={option}>{option}</p>)
                }
                
            </div>
        );
    }
}

class AddOption extends React.Component {
    handleAddOption(e) {
        e.preventDefault();

        const option = e.target.elements.option.value.trim();

        if (option) {
            alert(option);
        }
    }
    render() {
        return (
            <div>
                <div className="form-group">
                    <form onSubmit={this.handleAddOption}>
                        <input type="text" name="option"/>
                        <button>Submit</button>
                    </form>
                </div>
            </div>
        );
    }
}

// class Option extends React.Component {
//     render() {
//         return (
//             <div>
//                 Option 1
//             </div>
//         );
//     }
// }

ReactDOM.render(<IndecisionApp/>, document.getElementById('app'));